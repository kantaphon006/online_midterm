﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using Photon.Pun;
using UnityEditor;

public class Player : MonoBehaviourPun
{
   
    public float movementspeed;

    public GameObject camera;
    public GameObject BulletSpawnPoint;
    
    public float waitTime;
    public float BulletForce = 20f;
    public GameObject Bullet;
     void Start()
     {
        
     }

    

    private void Update()
    {
       
        
        Plane playerPlane = new Plane(Vector3.up, transform.position);
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        float hitDist = 0.0f;

        if (playerPlane.Raycast(ray,out hitDist))
        {
            Vector3 targetPoint = ray.GetPoint(hitDist);
            Quaternion targetRotation = Quaternion.LookRotation(targetPoint - transform.position);
            targetRotation.x = 0;
            targetRotation.z = 0;
           transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, 7f * Time.deltaTime);

            if (Input.GetMouseButtonDown(0))
            {
                CmdFire(BulletSpawnPoint.transform.rotation);
            }
        }
       
        
    }

    private void FixedUpdate()
   {
      // if (photonView.IsMine)
    //    {
            PlayerMovement();
    //    }
        
    }

    void PlayerMovement()
    {
        if (Input.GetKey(KeyCode.W))
    {
        transform.Translate(Vector3.forward *movementspeed*Time.deltaTime);
    }
        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(Vector3.left *movementspeed*Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(Vector3.right *movementspeed*Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(Vector3.back *movementspeed*Time.deltaTime);
        }
    }
    
        void CmdFire(Quaternion rotation)
        {
            //Keep Data when Instantiate.
            object[] data = { photonView.ViewID };

            // Spawn the bullet on the Clients and Create the Bullet from the Bullet Prefab
            Rigidbody bullet = PhotonNetwork.Instantiate(this.Bullet.name
                , this.transform.position + (this.transform.forward * 1.5f)
                , rotation
                , 0
                , data).GetComponent<Rigidbody>();

            // Add velocity to the bullet
            bullet.velocity = bullet.transform.forward * BulletForce;

            // Destroy the bullet after 10 seconds
            Destroy(bullet.gameObject, 10.0f);
        }
    
}
