﻿using Photon.Pun;
using UnityEngine;

[RequireComponent(typeof(PhotonTransformView))]
[RequireComponent(typeof(PhotonRigidbodyView))]
public class PunBullet : MonoBehaviourPun , IPunInstantiateMagicCallback
{
    [Range(1, 10)] public int m_AmountDamage = 5;
    public float BulletForce = 10000f;
    private int OwnerViewID = -1;
    public void OnPhotonInstantiate(PhotonMessageInfo info)
    {
        // e.g. store this gameobject as this player's charater in Player.TagObject
        info.Sender.TagObject = this.gameObject;
        OwnerViewID = info.photonView.ViewID;
        //info.sender.TagObject = this.GameObject;
        Rigidbody bullet = GetComponent<Rigidbody>();
        bullet.velocity = bullet.transform.forward * BulletForce;
        if(!photonView.IsMine)
            return;
        Destroy(this.gameObject,10.0f);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!photonView.IsMine)
            return;
        if (collision.gameObject.CompareTag("Player"))
        {
            Debug.Log("Bullet Collision to other player.");
            PunUserNetControl tempOther = collision.gameObject.GetComponent<PunUserNetControl>();
            
            if(tempOther!=null)
                Debug.Log("Attack to other ViewID :"+tempOther.photonView.ViewID);

            PunHealth tempHealthOther = collision.gameObject.GetComponent<PunHealth>();
            if(tempHealthOther !=null)
                tempHealthOther.TakeDamage(m_AmountDamage,photonView.ViewID);
        }

        Destroy(this.gameObject);        
    }

    private void OnDestroy()
    {
        if (!photonView.IsMine)
            return;

        PhotonNetwork.Destroy(this.gameObject);
    }
}
