﻿using ExitGames.Client.Photon;
using UnityEngine;
using Photon.Pun;
using Photon.Pun.UtilityScripts;
using Photon.Realtime;



public class PunNetworkManager : ConnectAndJoinRandom 
{
    public static PunNetworkManager singleton;

    [Header("Spawn Info")]
    [Tooltip("The prefab to use for representing the player")]
    public GameObject GamePlayerPrefab;

    public bool isGameStart = false;
    public bool isFirstSetting = false;
    public bool isGameOver = false;
   // public GameObject HealingPrefab;
    public int numberOfHealing = 5;
    float m_count = 0;
    public float m_CountDownDropHeal = 10;
    /// <summary>
    /// Create delegate Method
    /// </summary>
    public delegate void PlayerSpawned();
    public static event PlayerSpawned OnPlayerSpawned;

    public delegate void FirstSetting();
    

 
    private void Awake() {
        singleton = this;

        //Add Reference Method to Delegate Method
        OnPlayerSpawned += SpawnPlayer;
       
    }

    private void Update() {

        if(PhotonNetwork.IsMasterClient != true)
            return;

        
    }
    

    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();
        Camera.main.gameObject.SetActive(true);

        //Delegate Function Call when Player Joined to Room.
        OnPlayerSpawned();
    }

    public void SpawnPlayer()
    {
        if (PunUserNetControl.LocalPlayerInstance == null)
        {
            Debug.Log("We are Instantiating LocalPlayer from " + SceneManagerHelper.ActiveSceneName);
            //PunNetworkManager.singleton.SpawnPlayer();
            // we're in a room. spawn a character for the local player. it gets synced by using PhotonNetwork.Instantiate
            PhotonNetwork.Instantiate(GamePlayerPrefab.name,
                new Vector3(0f, 5f, 0f), Quaternion.identity, 0);

            isGameStart = true;
        }
        else
        {
            Debug.Log("Ignoring scene load for " + SceneManagerHelper.ActiveSceneName);
        }

    }

    public override void OnDisconnected(DisconnectCause cause) {
        base.OnDisconnected(cause);

        if(Camera.main != null)
            Camera.main.gameObject.SetActive(true);
    }

}
